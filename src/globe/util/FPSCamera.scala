package globe.util

import org.joml.{Matrix4f, Vector2f, Vector3f}
import org.lwjgl.glfw.GLFW

object FPSCamera {
  val movescale = 0.05f
  val rotscale = 0.0025f

  var mousedown = false
  var mousepos = new Vector2f(-1.0f, -1.0f)

  var campos = new Vector3f(0.0f, 0.0f, -3.0f)
  var camvel = new Vector3f(0.0f, 0.0f, 0.0f)
  var camrot = new Vector2f(0.0f, 0.0f)

  def keyPressed(key: Int, action: Int): Unit = {
    if (key == GLFW.GLFW_KEY_W || key == GLFW.GLFW_KEY_S) {
      camvel.z = action * movescale * (if (key == GLFW.GLFW_KEY_W) 1.0f else -1.0f)
    }
  }

  def mouseClicked(button: Int, action: Int, mods: Int): Unit = {
    if (button == 0) {
      mousedown = (action == 1)
    }
  }

  def mouseMove(xpos: Double, ypos: Double): Unit = {
    val valid = mousepos.x >= 0.0f
    val diffx = xpos.toFloat - mousepos.x
    val diffy = ypos.toFloat - mousepos.y
    mousepos.set(xpos.toFloat, ypos.toFloat)

    if (valid && mousedown) {
      camrot.x += diffy
      camrot.y += diffx
    }
  }

  def update(mat: Matrix4f, time: Float): Unit = {
    campos.x += camvel.x * time
    campos.y += camvel.y * time
    campos.z += camvel.z * time

    mat.identity()
    mat.translate(campos.x, campos.y, campos.z)
    mat.rotateX(camrot.x * rotscale)
    mat.rotateY(camrot.y * rotscale)
  }
}
