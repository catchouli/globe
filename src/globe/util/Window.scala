package globe.util

import globe.rendering.{Renderer, ShaderWatchService}
import org.lwjgl.glfw.GLFW._
import org.lwjgl.glfw.{GLFWCursorPosCallback, GLFWErrorCallback, GLFWKeyCallback, GLFWMouseButtonCallback, _}
import org.lwjgl.system.MemoryUtil._

import scalaz.Scalaz._
import scalaz.{\/, _}
import scala.collection.JavaConverters._

abstract class Window {
  def main(args: Array[String]): Unit = withWindow(runGame)

  def withWindow(fun: Long => \/[String,Unit]) = for {
    _ <- GLFWErrorCallback.createPrint(System.err).set().right
    _ <- glfwInit().either().or("Failed to initialise GLFW")
    _ <- glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE).right
    _ <- glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE).right
    win <- Option(glfwCreateWindow(1080, 1080, "Gimp", NULL, NULL))
      .toRightDisjunction("Failed to create window")
    res <- fun(win)
  } yield res

  object keyPressed extends GLFWKeyCallback {
    override def invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int): Unit = {
      FPSCamera.keyPressed(key, action)
    }
  }

  object mouseClicked extends GLFWMouseButtonCallback {
    override def invoke(window: Long, button: Int, action: Int, mods: Int): Unit = {
      FPSCamera.mouseClicked(button, action, mods)
    }
  }

  object mouseMoved extends GLFWCursorPosCallback {
    override def invoke(window: Long, xpos: Double, ypos: Double): Unit = {
      FPSCamera.mouseMove(xpos, ypos)
    }
  }

  def runGame(win: Long): \/[String, Unit] = {
    // install handlers
    glfwSetKeyCallback(win, keyPressed)
    glfwSetMouseButtonCallback(win, mouseClicked)
    glfwSetCursorPosCallback(win, mouseMoved)

    // Init renderer
    Renderer.impl.init(win)
    glfwShowWindow(win)

    if (Resources.catsDevBuild) {
      glfwSetWindowPos(win, 0, 420)
    }

    var lastFpsUpdate = glfwGetTime
    var frameCount = 0

    while (!glfwWindowShouldClose(win)) {
      val currentTime = glfwGetTime
      frameCount = frameCount + 1

      // Count FPS
      if (currentTime - lastFpsUpdate > 1.0) {
        println(s"fps: $frameCount")
        frameCount = 0
        lastFpsUpdate = currentTime
      }

      // Handle input and render
      glfwPollEvents
      render(glfwGetTime.toFloat)
      glfwSwapBuffers(win)
    }

    ShaderWatchService.running = false

    return \/-()
  }

  def render(time: Float)
}
