package globe.util

import de.javagl.obj.{ObjData, ObjReader, ObjUtils}
import globe.rendering.api.{IndexBuffer, VertexBuffer}

class Model(filename: String) {
  val vbo = new VertexBuffer {
    var count = 0

    val positionOffset = 0
    val positionSize = 3
    val normalOffset = positionOffset + 4 * positionSize
    val normalSize = 3
    val uvOffset = normalOffset + 4 * normalSize
    val uvSize = 2
    val stride = 4 * positionSize + 4 * normalSize + 4 * uvSize
  }

  val ibo = new IndexBuffer {
    var count = 0
  }

  {
    val is = Resources.getStream(filename)
    val obj = ObjUtils.convertToRenderable(ObjReader.read(is))

    val indices = ObjData.getFaceVertexIndices(obj)
    val vertices = ObjData.getVertices(obj)
    val texCoords = ObjData.getTexCoords(obj, 2)
    val normals = ObjData.getNormals(obj)

    ibo.count = indices.capacity()
    for (x <- 0 until ibo.count) {
      ibo.data += indices.get()
    }

    vbo.count = vertices.capacity() / 3
    for (x <- 0 until vbo.count) {
      vbo.data += vertices.get()
      vbo.data += vertices.get()
      vbo.data += vertices.get()
      vbo.data += normals.get()
      vbo.data += normals.get()
      vbo.data += normals.get()
      vbo.data += texCoords.get()
      vbo.data += texCoords.get()
    }
  }
}
