package globe.util

import java.io.{FileInputStream, InputStream}
import java.nio.file.{Path, Paths}

object Resources {
  val resourcesDir = "resources"
  val isDevel = System.getProperty("java.class.path").contains("idea_rt")
  val catsDevBuild = isDevel && "cat".equals(System.getenv("LOGNAME"))

  def getStream(path: String): InputStream = {
    if (isDevel)
      new FileInputStream(s"$resourcesDir/$path")
    else
      Thread.currentThread().getContextClassLoader().getResourceAsStream(path)
  }

  def getSource(path: String): io.BufferedSource = {
    if (isDevel)
      io.Source.fromFile(s"$resourcesDir/$path")
    else
      io.Source.fromResource(path)
  }

  def path(path: String): Path = {
    Paths.get(if (isDevel) s"resources/$path" else path)
  }
}
