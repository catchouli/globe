package globe.renderable

import globe.rendering.api._
import globe.shaders.basic.Lit
import globe.shaders.basic.Lit
import globe.shaders.standard.ForwardShader
import org.joml.{Matrix4f, Vector3f}

object Isocahedron extends Renderable {
  val buf = new VertexBuffer {
    case class Face(a: Int, b: Int, c: Int)
    case class Vert(x: Float, y: Float, z: Float)

    private val faces = Array(
      Face(2, 1, 0),
      Face(3, 2, 0),
      Face(4, 3, 0),
      Face(5, 4, 0),
      Face(1, 5, 0),
      Face(11, 6,  7),
      Face(11, 7,  8),
      Face(11, 8,  9),
      Face(11, 9,  10),
      Face(11, 10, 6),
      Face(1, 2, 6),
      Face(2, 3, 7),
      Face(3, 4, 8),
      Face(4, 5, 9),
      Face(5, 1, 10),
      Face(2,  7, 6),
      Face(3,  8, 7),
      Face(4,  9, 8),
      Face(5, 10, 9),
      Face(1, 6, 10)
    )

    private val verts = Array (
      new Vector3f(0.000f,  0.000f,  1.000f),
      new Vector3f(0.894f,  0.000f,  0.447f),
      new Vector3f(0.276f,  0.851f,  0.447f),
      new Vector3f(-0.724f,  0.526f,  0.447f),
      new Vector3f(-0.724f, -0.526f,  0.447f),
      new Vector3f(0.276f, -0.851f,  0.447f),
      new Vector3f(0.724f,  0.526f, -0.447f),
      new Vector3f(-0.276f,  0.851f, -0.447f),
      new Vector3f(-0.894f,  0.000f, -0.447f),
      new Vector3f(-0.276f, -0.851f, -0.447f),
      new Vector3f(0.724f, -0.526f, -0.447f),
      new Vector3f(0.000f,  0.000f, -1.000f)
    )

    faces.foreach { face =>
      val a = verts(face.a)
      val b = verts(face.b)
      val c = verts(face.c)

      val v = new Vector3f
      val w = new Vector3f
      b.sub(a, v)
      c.sub(a, w)

      val n = new Vector3f
      v.cross(w, n)

      data ++= Array(a.x, a.y, a.z, n.x, n.y, n.z,
        b.x, b.y, b.z, n.x, n.y, n.z,
        c.x, c.y, c.z, n.x, n.y, n.z)
    }

    val count = faces.length * 3
  }

  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    buf.bind(VertexBuffer.VertexBuffer)

    val shader = ForwardShader

    StateManager.withState(Seq()) { () =>
      shader.bind
      shader.pos.enable
      shader.nrm.enable
      shader.pos.bind(3, 24, 0)
      shader.nrm.bind(3, 24, 12)

      shader.proj.set(proj)
      shader.view.set(view)
      shader.obj.set(obj)

      shader.drawArrays(Primitive.Triangles, 0, buf.count)
      shader.pos.disable
      shader.nrm.disable
      shader.unbind
    }
  }
}
