package globe.renderable

import com.flowpowered.noise.module.source.Perlin
import globe.rendering.api.{VertexBuffer, Primitive, Renderable}
import globe.shaders.basic.Basic
import org.joml.{Matrix4f, Vector2f}

object CrappyCircle extends Renderable {

  val buf = new VertexBuffer() {
    private val radius = 0.5f
    private val vertexCount = 100

    private val perlin = new Perlin()
    private def displacement(v: Float) = perlin.getValue(v, 0, 0).toFloat * 0.25f
    private val vertex = (f:Float, displacement: Float => Float) => {
      val angle = f * 2.0f * Math.PI.toFloat
      val disp = radius + displacement(f)
      new Vector2f(Math.sin(angle).toFloat * disp, Math.cos(angle).toFloat * disp)
    }
    private val verts = (1 to vertexCount).map(i => vertex(i.toFloat / vertexCount, displacement))
    private val baseVerts = (1 to vertexCount).map(i => vertex(i.toFloat / vertexCount, a => {0.0f}))

    val baseCount = baseVerts.length
    val crappyCount = verts.length

    this.data ++= baseVerts.map(v => Seq(v.x, v.y)).flatten
    this.data ++= verts.map(v => Seq(v.x, v.y)).flatten
  }


  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    buf.bind(VertexBuffer.VertexBuffer)

    Basic.bind
    Basic.pos.enable
    Basic.pos.bind(2, 8, 0)
    Basic.proj.set(proj)
    Basic.view.set(view)
    Basic.drawArrays(Primitive.LineLoop, 0, buf.baseCount)
    Basic.drawArrays(Primitive.LineLoop, buf.baseCount, buf.crappyCount)
    Basic.obj.set(obj)
    Basic.pos.disable
    Basic.unbind
  }
}
