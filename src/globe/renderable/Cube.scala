package globe.renderable

import java.nio.file.Paths

import globe.rendering.api._
import globe.shaders.basic.Textured
import globe.shaders.basic.Color
import org.joml.Matrix4f

object Cube extends Renderable {
  val cube = new VertexBuffer {
    data ++= Array(
       1.0f, 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      -1.0f, 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
       1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
       1.0f,-1.0f, 1.0f, 1.0f, 0.5f, 0.0f, 1.0f, 1.0f,
      -1.0f,-1.0f, 1.0f, 1.0f, 0.5f, 0.0f, 1.0f, 0.0f,
      -1.0f,-1.0f,-1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 0.0f,
       1.0f,-1.0f,-1.0f, 1.0f, 0.5f, 0.0f, 0.0f, 1.0f,
       1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
       1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
       1.0f,-1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      -1.0f,-1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -1.0f, 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,
       1.0f, 1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      -1.0f, 1.0f,-1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -1.0f,-1.0f,-1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
      -1.0f,-1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
       1.0f, 1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f,
       1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f,
       1.0f,-1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
       1.0f,-1.0f,-1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f
    )
  }

  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    cube.bind(VertexBuffer.VertexBuffer)

    val shader = Color

    shader.bind
    shader.pos.enable
    shader.col.enable
    shader.pos.bind(3, 32, 0)
    shader.col.bind(3, 32, 12)

    shader.proj.set(proj)
    shader.view.set(view)
    shader.obj.set(obj)

    shader.drawArrays(Primitive.Quads, 0, 24)
    shader.pos.disable
    shader.col.disable
    shader.unbind
  }
}
