package globe.renderable

import java.io.FileInputStream
import java.nio.ByteOrder
import java.nio.file.Paths

import globe.rendering.api._
import globe.util.EndianDataInputStream
import org.joml._

object Skybox extends Renderable {
  val buf = new VertexBuffer {
    this.data ++= Array(
      -1.0f, -1.0f, 0.0f, 0.0f,
       1.0f, -1.0f, 1.0f, 0.0f,
       1.0f,  1.0f, 1.0f, 1.0f,
      -1.0f,  1.0f, 0.0f, 1.0f
    )
  }

  object CubeShader extends Shader {
    override val vertexSource = Some("shaders/cubemapbg.glsl")
    override val fragmentSource = Some("shaders/cubemapbg.glsl")

    case object pos extends ShaderAttrib
    case object uv extends ShaderAttrib
    override val shaderAttribs = Seq(pos, uv)

    case object proj extends ShaderUniform[Matrix4f]
    case object view extends ShaderUniform[Matrix4f]
    case object tex extends ShaderUniform[Texture.Unit]
    override val shaderUniforms = Seq(proj, view, tex)
  }

  val tex = new Texture {
    this.loadCubemap("textures/milkyway")
  }

  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    buf.bind(VertexBuffer.VertexBuffer)

    val unit = StateManager.pushTexture(tex)

    CubeShader.bind
    CubeShader.pos.enable
    CubeShader.uv.enable
    CubeShader.pos.bind(2, 16, 0)
    CubeShader.uv.bind(2, 16, 8)

    CubeShader.proj.set(proj)
    CubeShader.view.set(view)
    CubeShader.tex.set(unit)

    StateManager.withState(DepthWrite(false)) { () =>
      CubeShader.drawArrays(Primitive.Quads, 0, 4)
    }
    CubeShader.pos.disable
    CubeShader.uv.disable
    CubeShader.unbind

    StateManager.popTexture(tex, unit)
  }
}
