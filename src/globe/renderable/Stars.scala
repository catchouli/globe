package globe.renderable

import java.io.FileInputStream
import java.nio.ByteOrder

import globe.rendering.api._
import globe.util.{EndianDataInputStream, Resources}
import org.joml._

object Stars extends Renderable {
  val cacheFile = "data/stars"

  object StarShader extends Shader {
    override val vertexSource = Some("shaders/starfield.glsl")
    override val fragmentSource = Some("shaders/starfield.glsl")

    case object pos extends ShaderAttrib
    case object col extends ShaderAttrib
    override val shaderAttribs = Seq(pos, col)

    case object proj extends ShaderUniform[Matrix4f]
    case object view extends ShaderUniform[Matrix4f]
    override val shaderUniforms = Seq(proj, view)
  }

  val stars = new VertexBuffer {
    private val dis = new EndianDataInputStream(Resources.getStream(cacheFile))
    dis.order(ByteOrder.LITTLE_ENDIAN)

    // read the file version, then number of stars, and then read 8 values per star
    // layer, x, y, z, r, g, b, intensity
    val _ = dis.readInt // ^ file version
    val starCount = dis.readInt
    this.data ++= (1 to (8 * starCount) map { _ => dis.readFloat })
  }

  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    stars.bind(VertexBuffer.VertexBuffer)

    StarShader.bind
    StarShader.pos.enable
    StarShader.col.enable
    StarShader.pos.bind(3, 32, 4)
    StarShader.col.bind(4, 32, 16)

    StarShader.proj.set(proj)
    StarShader.view.set(view)

    StateManager.withState(DepthWrite(false)) { () =>
      StarShader.drawArrays(Primitive.Points, 0, stars.starCount)
    }
    StarShader.pos.disable
    StarShader.col.disable
    StarShader.unbind
  }
}
