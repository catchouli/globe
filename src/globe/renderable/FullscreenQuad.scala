package globe.renderable

import globe.rendering.api._
import globe.shaders.basic.Textured
import org.joml._

object FullscreenQuad extends Renderable {
  val buf = new VertexBuffer {
    this.data ++= Array(
      -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
       1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
       1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
      -1.0f,  1.0f, 0.0f, 0.0f, 1.0f
    )
  }

  var tex: Texture = _

  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f) = {
    buf.bind(VertexBuffer.VertexBuffer)

    val unit = StateManager.pushTexture(tex)

    val shader = Textured

    shader.bind
    shader.pos.enable
    shader.uv.enable
    shader.pos.bind(3, 20, 0)
    shader.uv.bind(2, 20, 12)

    shader.proj.set(proj)
    shader.view.set(view)
    shader.tex.set(unit)

    shader.drawArrays(Primitive.Quads, 0, 4)
    shader.pos.disable
    shader.uv.disable
    shader.unbind

    StateManager.popTexture(tex, unit)
  }
}
