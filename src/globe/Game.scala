package globe

import de.javagl.obj.{ObjData, ObjReader, ObjUtils}
import globe.renderable._
import globe.rendering.Renderer
import globe.rendering.api._
import globe.rendering.gl.GLTextureImpl
import globe.shaders.standard.ForwardShader
import globe.util.{FPSCamera, Model, Resources, Window}
import org.joml.{Matrix4f, Vector3f}
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30._

import scala.language.experimental.macros

object Game extends Window {
  Renderer.impl = globe.rendering.gl.GLRendererImpl

  def projection = new Matrix4f {
    this.perspective(Math.toRadians(60.0f).toFloat, 1.0f, 0.01f, 10000.0f)
  }

  val view = new Matrix4f {
    def update(time: Float) = FPSCamera.update(this, time)
  }

  val identity = new Matrix4f { this.identity }

  val states = Seq(DepthTest(true), ClearColor(0.0f, 0.0f, 0.0f, 1.0f))

  lazy val (framebuffer, colorBuf) = {
    val colorBuf = new Texture
    val normalBuf = new Texture
    val depthBuf = new Texture

    colorBuf.set(Texture.Texture2D, Texture.ColorRGBA, 1024, 1024, None, None)
    normalBuf.set(Texture.Texture2D, Texture.ColorRGBA, 1024, 1024, None, None)
    depthBuf.set(Texture.Texture2D, Texture.DepthStencil, 1024, 1024, None, None)

    val framebuffer = new Framebuffer
    framebuffer.attach(Framebuffer.Color0, colorBuf)
    framebuffer.attach(Framebuffer.Color1, normalBuf)
    framebuffer.attach(Framebuffer.DepthStencil, depthBuf)

    if (!framebuffer.validate)
      throw new RuntimeException("framebuffer not complete")

    (framebuffer, colorBuf)
  }

  val font = {
  }

  lazy val model = new Model("meshes/Room.obj")

  def render(time: Float) = StateManager.withState(states) { () =>
    Renderer.frame(() => {
      view.update(time)

      val cubeMatrix = new Matrix4f
      cubeMatrix.identity
      cubeMatrix.translate(-4.0f, 0.0f, -9.0f)
      cubeMatrix.rotate(time, new Vector3f(0.0f, 1.0f, 0.0f))

      val isocahedronMatrix = new Matrix4f
      isocahedronMatrix.identity
      //isocahedronMatrix.rotate(time, new Vector3f(1.0f, 0.0f, 1.0f).normalize())
      isocahedronMatrix.scale(0.1f)

      framebuffer.bind

      Renderer.frame { () =>
        Skybox.render(projection, view, identity)
        Stars.render(projection, view, identity)
        Cube.render(projection, view, cubeMatrix)
        CrappyCircle.render(projection, view, identity)
        //Isocahedron.render(projection, view, isocahedronMatrix)

        model.vbo.bind(VertexBuffer.VertexBuffer)
        model.ibo.bind(VertexBuffer.IndexBuffer)
        ForwardShader.bind
        ForwardShader.pos.enable
        ForwardShader.nrm.enable
        ForwardShader.uv.enable
        ForwardShader.pos.bind(model.vbo.positionSize, model.vbo.stride, model.vbo.positionOffset)
        ForwardShader.nrm.bind(model.vbo.normalSize, model.vbo.stride, model.vbo.normalOffset)
        ForwardShader.uv.bind(model.vbo.uvSize, model.vbo.stride, model.vbo.uvOffset)
        ForwardShader.proj.set(projection)
        ForwardShader.view.set(view)
        ForwardShader.obj.set(isocahedronMatrix)
        println(s"${model.ibo.count}")
        ForwardShader.drawElements(Primitive.Triangles, model.ibo.count)
        ForwardShader.pos.disable
        ForwardShader.nrm.disable
        ForwardShader.uv.disable
        ForwardShader.unbind

        Renderer.impl.checkErr
      }

      framebuffer.unbind

      FullscreenQuad.tex = colorBuf
      FullscreenQuad.render(identity, identity, identity)
    })
  }
}
