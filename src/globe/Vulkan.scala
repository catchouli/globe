package globe

import globe.util.Window

import scalaz.{\/, \/-}
import org.lwjgl.glfw.GLFW._
import org.lwjgl.system.MemoryUtil._
import org.lwjgl.vulkan._
import EXTDebugReport._
import KHRSurface._
import KHRSwapchain._
import VK10._

object Vulkan {
  val win = new Window {
    override def render(time: Float): Unit = {}
  }

  def main(args: Array[String]): Unit = {
    win.withWindow(run)
  }

  def run(win: Long): \/[String, Unit] = {
    glfwShowWindow(win)

    val allocator = VkAllocationCallbacks.calloc()
    val pp = memAllocPointer(1)

    allocator
      .pfnAllocation(new VkAllocationFunction() {
        override def invoke(pUserData: Long, size: Long, alignment: Long, allocationScope: Int): Long = nmemAlignedAlloc(alignment, size)
      })
      .pfnFree(new VkFreeFunction() {
        override def invoke(pUserData: Long, pMemory: Long): Unit = nmemAlignedFree(pMemory)
      })
      .pfnReallocation(new VkReallocationFunction() {
        override def invoke(pUserData: Long, pOriginal: Long, size: Long, alignment: Long, allocationScope: Int): Long = {
          nmemAlignedFree(pOriginal)
          nmemAlignedAlloc(alignment, size)
        }
      })

    // Step 1 - Create instance
    val app = VkApplicationInfo.malloc()
      .sType(VK_STRUCTURE_TYPE_APPLICATION_INFO)
      .pNext(NULL)
      .pApplicationName(memUTF8("test"))
      .applicationVersion(0)
      .pEngineName(memUTF8("test"))
      .engineVersion(0)
      .apiVersion(VK_API_VERSION_1_0)

    val instInfo = VkInstanceCreateInfo.malloc()
      .sType(VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
      .pNext(NULL)
      .flags(0)
      .pApplicationInfo(app)

    val err = vkCreateInstance(instInfo, allocator, pp)

    println(s"attempt to create vulkan instance: $err")

    return \/-()
  }
}
