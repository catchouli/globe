package globe.shaders.basic

import globe.rendering.api.{Shader, ShaderAttrib, ShaderUniform}
import org.joml.Matrix4f

object Color extends Shader {
  override val vertexSource = Some("shaders/color.glsl")
  override val fragmentSource = Some("shaders/color.glsl")

  case object pos extends ShaderAttrib
  case object col extends ShaderAttrib
  override val shaderAttribs = Seq(pos, col)

  case object proj extends ShaderUniform[Matrix4f]
  case object view extends ShaderUniform[Matrix4f]
  case object obj extends ShaderUniform[Matrix4f]
  override val shaderUniforms = Seq(proj, view, obj)
}