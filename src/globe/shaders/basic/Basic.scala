package globe.shaders.basic

import globe.rendering.api.{Shader, ShaderAttrib, ShaderUniform}
import org.joml.Matrix4f

object Basic extends Shader {
  override val vertexSource = Some("shaders/basic.glsl")
  override val fragmentSource = Some("shaders/basic.glsl")

  case object pos extends ShaderAttrib
  override val shaderAttribs = Seq(pos)

  case object proj extends ShaderUniform[Matrix4f]
  case object view extends ShaderUniform[Matrix4f]
  case object obj extends ShaderUniform[Matrix4f]
  override val shaderUniforms = Seq(proj, view, obj)
}