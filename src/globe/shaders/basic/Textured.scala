package globe.shaders.basic

import globe.rendering.api.{Shader, ShaderAttrib, ShaderUniform, Texture}
import org.joml.Matrix4f

object Textured extends Shader {
  override val vertexSource = Some("shaders/textured.glsl")
  override val fragmentSource = Some("shaders/textured.glsl")

  case object pos extends ShaderAttrib
  case object uv extends ShaderAttrib
  override val shaderAttribs = Seq(pos, uv)

  case object proj extends ShaderUniform[Matrix4f]
  case object view extends ShaderUniform[Matrix4f]
  case object obj extends ShaderUniform[Matrix4f]
  case object tex extends ShaderUniform[Texture.Unit]
  override val shaderUniforms = Seq(proj, view, obj, tex)
}