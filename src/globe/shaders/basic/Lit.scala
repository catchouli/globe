package globe.shaders.basic

import globe.rendering.api.{Shader, ShaderAttrib, ShaderUniform}
import org.joml.Matrix4f

object Lit extends Shader {
  override val vertexSource = Some("shaders/lit.glsl")
  override val fragmentSource = Some("shaders/lit.glsl")

  case object pos extends ShaderAttrib
  case object nrm extends ShaderAttrib
  override val shaderAttribs = Seq(pos, nrm)

  case object proj extends ShaderUniform[Matrix4f]
  case object view extends ShaderUniform[Matrix4f]
  case object obj extends ShaderUniform[Matrix4f]
  override val shaderUniforms = Seq(proj, view, obj)
}