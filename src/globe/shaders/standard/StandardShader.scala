package globe.shaders.standard

import globe.rendering.api.{Shader, ShaderAttrib, ShaderUniform}
import org.joml.Matrix4f

abstract class StandardShader extends Shader {
  case object pos extends ShaderAttrib
  case object nrm extends ShaderAttrib
  case object col extends ShaderAttrib
  case object uv extends ShaderAttrib
  override val shaderAttribs = Seq(pos, nrm)

  case object proj extends ShaderUniform[Matrix4f]
  case object view extends ShaderUniform[Matrix4f]
  case object obj extends ShaderUniform[Matrix4f]
  override val shaderUniforms = Seq(proj, view, obj)
}
