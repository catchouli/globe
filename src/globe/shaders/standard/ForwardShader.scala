package globe.shaders.standard

object ForwardShader extends StandardShader {
  override val vertexSource = Some("shaders/forward.glsl")
  override val fragmentSource = Some("shaders/forward.glsl")
}
