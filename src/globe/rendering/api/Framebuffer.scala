package globe.rendering.api

import globe.rendering.Renderer

trait FramebufferImpl {
  def bind
  def unbind
  def attach(attachment: Framebuffer.Attachment, tex: Texture)
  def detach(attachment: Framebuffer.Attachment)
  def validate: Boolean
}

class Framebuffer {
  val impl = Renderer.impl.createFramebuffer

  def bind = impl.bind
  def unbind = impl.unbind

  def attach(attachment: Framebuffer.Attachment, tex: Texture) = impl.attach(attachment, tex)
  def detach(attachment: Framebuffer.Attachment) = impl.detach(attachment)

  def validate = impl.validate
}

object Framebuffer {
  sealed trait Attachment
  case object Color0 extends Attachment
  case object Color1 extends Attachment
  case object Color2 extends Attachment
  case object Color3 extends Attachment
  case object Depth extends Attachment
  case object DepthStencil extends Attachment
}
