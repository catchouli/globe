package globe.rendering.api

import globe.rendering.Renderer

import scala.collection.mutable

sealed trait State
case class DepthTest(enabled: Boolean) extends State
case class DepthWrite(enabled: Boolean) extends State
case class ClearColor(r: Float, g: Float, b: Float, a: Float) extends State
case class Fill(enabled: Boolean) extends State

trait StateImpl {
  def applyState(state: State)
}

object StateManager {
  val impl: StateImpl = Renderer.impl.createStateImpl

  private val stateMap = mutable.Map.empty[Class[_], TraitDecl[_]]

  private val availableTextureUnits: mutable.Stack[Texture.Unit] = mutable.Stack(Texture.units: _*)

  def pushState(state: State) = {
    stateMap.getOrElse(state.getClass, null).push(state)
  }

  def popState(state: State) = {
    stateMap.getOrElse(state.getClass, null).pop
  }

  def withState(state: State)(func: () => Unit) = {
    pushState(state)
    func()
    popState(state)
  }

  def withState(state: Seq[State])(func: () => Unit) = {
    state.foreach(pushState(_))
    func()
    state.reverseIterator.foreach(popState(_))
  }

  def pushTexture(tex: Texture): Texture.Unit = {
    val unit = availableTextureUnits.pop
    tex.bind(unit)
    unit
  }

  def popTexture(tex: Texture, unit: Texture.Unit) = {
    tex.unbind(unit)
    availableTextureUnits.push(unit)
  }

  // Declarations of states
  sealed trait TraitDecl[A <: State] {
    val default: A
    var cur: A = default
    private val stack = mutable.Stack.empty[A]

    def init = {
      cur = default
      stateMap += (cur.getClass -> this)
      impl.applyState(cur)
    }

    def push(state: State) = {
      stack.push(cur)
      if (!cur.equals(state)) {
        cur = state.asInstanceOf[A]
        impl.applyState(cur)
      }
    }

    def pop: State = {
      val state = stack.pop
      if (!cur.equals(state)) {
        cur = state
        impl.applyState(cur)
      }
      cur
    }
  }

  object ClearColorDecl extends TraitDecl[ClearColor] {
    override val default = ClearColor(0.0f, 0.0f, 0.0f, 0.0f)
  }

  object DepthTestDecl extends TraitDecl[DepthTest] {
    override val default = DepthTest(false)
  }

  object DepthWriteDecl extends TraitDecl[DepthWrite] {
    override val default = DepthWrite(true)
  }

  object FillDecl extends TraitDecl[Fill] {
    override val default = Fill(true)
  }

  // Make sure to list all the states here so they get initialised
  ClearColorDecl.init
  DepthTestDecl.init
  DepthWriteDecl.init
  FillDecl.init
}