package globe.rendering.api

import globe.rendering.Renderer

trait UniformImpl[A] {
  def init(name: String)
  def set(value: A)
}

class ShaderUniform[A] {
  val impl: UniformImpl[A] = Renderer.impl.createShaderUniformImpl

  def set(value: A) = impl.set(value)
  def init = impl.init(this.toString)
}
