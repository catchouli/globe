package globe.rendering.api

import java.nio.ByteBuffer

import globe.rendering.Renderer
import globe.rendering.api.VertexBuffer.BindTarget
import org.lwjgl.BufferUtils

import scala.collection.mutable.ArrayBuffer


object VertexBuffer {
  trait BindTarget
  case object VertexBuffer extends BindTarget
  case object IndexBuffer extends BindTarget
}

trait BufferImpl {
  def upload(target: BindTarget, data: ByteBuffer)
  def bind(target: BindTarget)
}

abstract class GpuBuffer[T] {
  val impl: BufferImpl = Renderer.impl.createBufferImpl

  var dirty = true
  val data = ArrayBuffer[T]()

  def bind(target: BindTarget) = {
    if (dirty) {
      dirty = false
      impl.upload(target, this.toByteBuffer)
    }

    impl.bind(target)
  }

  def toByteBuffer: ByteBuffer
}

class VertexBuffer extends GpuBuffer[Float] {
  def toByteBuffer: ByteBuffer = {
    val buf = BufferUtils.createByteBuffer(4 * data.length)
    buf.asFloatBuffer.put(data.toArray)
    buf
  }
}

class IndexBuffer extends GpuBuffer[Int] {
  def toByteBuffer: ByteBuffer = {
    val buf = BufferUtils.createByteBuffer(4 * data.length)
    buf.asIntBuffer.put(data.toArray)
    buf
  }
}