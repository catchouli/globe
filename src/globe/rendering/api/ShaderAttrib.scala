package globe.rendering.api

import globe.rendering.Renderer

trait AttribImpl {
  def init(name: String)
  def enable()
  def disable()
  def bind(size: Int, stride: Int, offset: Int)
}

class ShaderAttrib {
  val impl: AttribImpl = Renderer.impl.createShaderAttribImpl

  def init = impl.init(this.toString)
  def enable = impl.enable
  def disable = impl.disable
  def bind(size: Int, stride: Int, offset: Int) = impl.bind(size, stride, offset)
}
