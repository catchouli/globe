package globe.rendering.api

import java.nio.file.Paths

import globe.rendering.{Renderer, ShaderWatchService}
import globe.util.Resources

object Primitive {
  sealed trait Type
  case object Points extends Type
  case object Triangles extends Type
  case object Quads extends Type
  case object LineLoop extends Type
}

trait ShaderImpl {
  def bind
  def unbind
  def drawArrays(prim: Primitive.Type, start: Int, count: Int)
  def drawElements(prim: Primitive.Type, count: Int)
  def build(shader: Shader): Boolean
}

abstract class Shader {
  val impl = Renderer.impl.createShaderImpl

  var dirty = true

  val vertexSource: Option[String]
  val fragmentSource: Option[String]

  val shaderUniforms: Seq[ShaderUniform[_]] = Seq()
  val shaderAttribs: Seq[ShaderAttrib] = Seq()

  def bind = {
    if (dirty) {
      vertexSource.foreach(a => ShaderWatchService.add(this, Resources.path(a)))
      fragmentSource.foreach(a => ShaderWatchService.add(this, Resources.path(a)))
      dirty = false
      impl.build(this)
    }
    impl.bind
  }

  def unbind = impl.unbind

  def drawArrays(a: Primitive.Type, start: Int, count: Int) = impl.drawArrays(a, start, count)
  def drawElements(a: Primitive.Type, count: Int) = impl.drawElements(a, count)
}
