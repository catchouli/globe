package globe.rendering.api

import org.joml.Matrix4f

trait Renderable {
  def render(proj: Matrix4f, view: Matrix4f, obj: Matrix4f)
}
