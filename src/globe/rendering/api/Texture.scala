package globe.rendering.api

import java.io.FileInputStream
import java.nio.ByteBuffer
import java.nio.file.Path

import globe.rendering.Renderer
import globe.util.Resources
import org.lwjgl.BufferUtils
import org.newdawn.slick.opengl.PNGDecoder

trait TextureImpl {
  def set(typ: Texture.Type, format: Texture.Format, width: Int, height: Int, cubemapFace: Option[Texture.Face], buf: Option[ByteBuffer])

  def bind(unit: Texture.Unit)
  def unbind(unit: Texture.Unit)

  def filter(min: Texture.Filter, max: Texture.Filter)
  def wrap(x: Texture.Wrap, y: Texture.Wrap, z: Texture.Wrap)

  def reset
}

object Texture {
  sealed trait Unit
  case object Unit0 extends Unit
  case object Unit1 extends Unit
  case object Unit2 extends Unit
  case object Unit3 extends Unit
  case object Unit4 extends Unit
  case object Unit5 extends Unit
  case object Unit6 extends Unit
  case object Unit7 extends Unit
  val units = Seq(Unit0, Unit1, Unit2, Unit3, Unit4, Unit5, Unit6, Unit7)

  sealed trait Filter
  case object Nearest extends Filter
  case object Linear extends Filter

  sealed trait Wrap
  case object Repeat extends Wrap
  case object Clamp extends Wrap

  sealed trait Type
  case object Texture2D extends Type
  case object Cubemap extends Type

  sealed trait Face
  case object PositiveX extends Face
  case object NegativeX extends Face
  case object PositiveY extends Face
  case object NegativeY extends Face
  case object PositiveZ extends Face
  case object NegativeZ extends Face

  sealed trait Format
  case object ColorRGBA extends Format
  case object ColorRGBA32F extends Format
  case object DepthStencil extends Format

  sealed trait DataType
  case object UnsignedInt extends DataType
}

class Texture {
  case class Image(width: Int, height: Int, buf: ByteBuffer)

  val impl = Renderer.impl.createTexture

  def load(filepath: String) = {
    val image = loadFile(filepath)
    if (image == null) {
      println(s"Failed to load texture $filepath")
    }
    else {
      impl.set(Texture.Texture2D, Texture.ColorRGBA, image.width, image.height, None, Some(image.buf))
    }
  }

  def loadCubemap(filepath: String) = {
    val paths = Seq(
      (Texture.PositiveX, filepath + "/X+.png"),
      (Texture.NegativeX, filepath + "/X-.png"),
      (Texture.PositiveY, filepath + "/Y+.png"),
      (Texture.NegativeY, filepath + "/Y-.png"),
      (Texture.PositiveZ, filepath + "/Z+.png"),
      (Texture.NegativeZ, filepath + "/Z-.png")
    )

    paths.foreach(path => {
      val image = loadFile(path._2)
      if (image == null) {
        println(s"Failed to load texture $path")
      }
      else {
        println(s"Loading texture $path")
        impl.set(Texture.Cubemap, Texture.ColorRGBA, image.width, image.height, Some(path._1), Some(image.buf))
      }
    })
  }

  def set(typ: Texture.Type, frmt: Texture.Format, width: Int, height: Int,
                   cubemapFace: Option[Texture.Face], buf: Option[ByteBuffer]) = {
    impl.set(typ, frmt, width, height, cubemapFace, buf)
  }

  def bind(unit: Texture.Unit) = impl.bind(unit)
  def unbind(unit: Texture.Unit) = impl.unbind(unit)

  def filter(min: Texture.Filter, max: Texture.Filter) = impl.filter(min, max)
  def wrap(x: Texture.Wrap, y: Texture.Wrap, z: Texture.Wrap) = impl.wrap(x, y, z)

  def reset = impl.reset

  private def loadFile(path: String): Image = {
    try {
      val decoder = new PNGDecoder(Resources.getStream(path))
      val stride = decoder.getWidth * 4
      val buf = BufferUtils.createByteBuffer(stride * decoder.getHeight)
      decoder.decode(buf, stride, PNGDecoder.RGBA)
      buf.flip
      Image(decoder.getWidth, decoder.getHeight, buf)
    }
    catch {
      case e: Exception => null
    }
  }
}
