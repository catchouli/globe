package globe.rendering

import globe.rendering.api._

trait RendererImpl {
  def init(win: Long): Unit
  def startFrame: Unit
  def endFrame: Unit

  def createBufferImpl: BufferImpl
  def createShaderImpl: ShaderImpl
  def createShaderUniformImpl[A]: UniformImpl[A]
  def createShaderAttribImpl: AttribImpl
  def createStateImpl: StateImpl
  def createTexture: TextureImpl
  def createFramebuffer: FramebufferImpl

  def checkErr: Unit
}

object Renderer {
  var impl: RendererImpl = null

  def frame(func: () => Unit): Unit = {
    impl.startFrame
    func()
    impl.endFrame
  }
}
