package globe.rendering

import java.nio.ByteBuffer

import globe.rendering.api._

import scala.collection.mutable.ArrayBuffer

object DummyRendererImpl extends RendererImpl {
  override def init(win: Long): Unit = println(s"Dummy renderer for window $win")
  override def startFrame: Unit = println("Dummy renderer frame start")
  override def endFrame: Unit = println("Dummy renderer frame end")
  override def checkErr: Unit = println("Dummy check error")

  override def createShaderImpl = new ShaderImpl {
    override def bind: Unit = println("Dummy shader bind")
    override def unbind: Unit = println("Dummy shader unbind")
    override def drawArrays(prim: Primitive.Type, start: Int, count: Int): Unit = println("Dummy shader draw")
    override def drawElements(prim: Primitive.Type, count: Int): Unit = println("Dummy shader draw")
    override def build(shader: Shader): Boolean = { println("Dummy shader build"); true }
  }

  override def createBufferImpl = new BufferImpl {
    override def upload(target: VertexBuffer.BindTarget, data: ByteBuffer) = println("Dummy buffer upload")
    override def bind(target: VertexBuffer.BindTarget) = println("Dummy buffer bind")
  }

  override def createShaderUniformImpl[A] = new UniformImpl[A] {
    override def init(name: String): Unit = println("Dummy uniform impl init")
    override def set(value: A): Unit = println("Dummy uniform impl set value")
  }

  override def createShaderAttribImpl = new AttribImpl {
    override def init(name: String): Unit = println("Dummy attrib impl init")
    override def bind(size: Int, stride: Int, offset: Int): Unit = println("Dummy attrib impl bind")
    override def enable(): Unit = println("Dummy attrib impl enable")
    override def disable(): Unit = println("Dummy attrib impl disable")
  }

  override def createStateImpl = new StateImpl {
    override def applyState(state: State): Unit = println("Dummy apply state")
  }

  override def createTexture = new TextureImpl {
    override def bind(unit: Texture.Unit): Unit = println("Dummy texture bind")
    override def unbind(unit: Texture.Unit): Unit = println("Dummy texture unbind")
    override def filter(min: Texture.Filter, max: Texture.Filter): Unit = println("Dummy texture filter")
    override def wrap(x: Texture.Wrap, y: Texture.Wrap, z: Texture.Wrap): Unit = println("Dummy texture wrap")
    override def reset: Unit = println("Dummy texture reset")

    override def set(typ: Texture.Type, format: Texture.Format, width: Int, height: Int,
       cubemapFace: Option[Texture.Face], buf: Option[ByteBuffer]): Unit = println("Dummy texture set")
  }

  override def createFramebuffer = new FramebufferImpl {
    override def bind: Unit = println("Dummy framebuffer bind")
    override def unbind: Unit = println("Dummy framebuffer unbind")
    override def detach(attachment: Framebuffer.Attachment): Unit = println("Dummy framebuffer attach")
    override def attach(attachment: Framebuffer.Attachment, tex: Texture): Unit = println("Dummy framebuffer detach")
    override def validate: Boolean = { println("Dummy framebuffer validate"); true }
  }
}
