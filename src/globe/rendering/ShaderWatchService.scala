package globe.rendering

import java.nio.file.{FileSystems, Files, Path, StandardWatchEventKinds}

import globe.rendering.api.Shader
import globe.util.Resources

import scala.collection.mutable
import scala.util.Try

object ShaderWatchService {
  val enabled = Resources.isDevel
  var running = enabled
  val watcher = FileSystems.getDefault.newWatchService
  val shaders: mutable.Set[(Shader, Path)] = mutable.Set()

  def add(s: Shader, p: Path) = {
    if (enabled) {
      p.getParent.register(watcher, StandardWatchEventKinds.ENTRY_MODIFY)
      ShaderWatchService.shaders += ((s, p))
    }
  }

  new Thread(() => {
    while (running) {
      val watchKey = watcher.poll(1000, java.util.concurrent.TimeUnit.MILLISECONDS)
      if (watchKey != null) {
        watchKey.pollEvents().forEach { event => {
          val path = watchKey.watchable().asInstanceOf[Path].resolve(event.context.asInstanceOf[Path])
          shaders.foreach { shader => {
            Try {
              if (Files.isSameFile(path.toAbsolutePath, shader._2.toAbsolutePath)) {
                println(s"shader ${path.getFileName} changed, recompiling")
                shader._1.dirty = true
              }
            }
          } }
        } }
        watchKey.reset
      }
    }
  }).start()
}
