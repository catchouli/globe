package globe.rendering.gl

import globe.rendering.api._
import org.lwjgl.opengl.GL11._

class GLStateImpl extends StateImpl {
  override def applyState(state: State) = state match {
    case col: ClearColor => glClearColor(col.r, col.g, col.b, col.a)
    case depth: DepthTest => glToggle(depth.enabled)(GL_DEPTH_TEST)
    case depth: DepthWrite => glDepthMask(depth.enabled)
    case fill: Fill => glPolygonMode(GL_FRONT_AND_BACK, if (fill.enabled) GL_FILL else GL_LINE)
  }

  private def glToggle(b: Boolean): (Int => Unit) = if (b) glEnable else glDisable
}
