package globe.rendering.gl

import globe.rendering.api.{Framebuffer, FramebufferImpl, Texture}
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL30._

class GLFramebufferImpl extends FramebufferImpl {
  val fbo = glGenFramebuffers()

  override def bind: Unit = {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo)
  }

  override def unbind: Unit = {
    glBindFramebuffer(GL_FRAMEBUFFER, 0)
  }

  override def attach(attachment: Framebuffer.Attachment, tex: Texture): Unit = {
    bind
    val impl = tex.impl.asInstanceOf[GLTextureImpl]
    glFramebufferTexture2D(GL_FRAMEBUFFER, glAttachment(attachment), GL_TEXTURE_2D, impl.id, 0)
    unbind
  }

  override def detach(attachment: Framebuffer.Attachment): Unit = {
    bind
    glFramebufferTexture2D(GL_FRAMEBUFFER, glAttachment(attachment), GL_TEXTURE_2D, 0, 0)
    unbind
  }

  private def glAttachment(attachment: Framebuffer.Attachment) = {
    attachment match {
      case Framebuffer.Color0 => GL_COLOR_ATTACHMENT0
      case Framebuffer.Color1 => GL_COLOR_ATTACHMENT1
      case Framebuffer.Color2 => GL_COLOR_ATTACHMENT2
      case Framebuffer.Color3 => GL_COLOR_ATTACHMENT3
      case Framebuffer.Depth => GL_DEPTH_ATTACHMENT
      case Framebuffer.DepthStencil => GL_DEPTH_STENCIL_ATTACHMENT
    }
  }

  override def validate = {
    bind
    val valid = glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE
    unbind
    valid
  }
}
