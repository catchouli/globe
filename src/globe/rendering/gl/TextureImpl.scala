package globe.rendering.gl

import java.nio.ByteBuffer

import globe.rendering.api.{Texture, TextureImpl}
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL12._
import org.lwjgl.opengl.GL13._
import org.lwjgl.opengl.GL30._

class GLTextureImpl extends TextureImpl {
  var id = -1
  var texType: Texture.Type = Texture.Texture2D
  var texFormat: Texture.Format = Texture.ColorRGBA
  var texWidth = 0
  var texHeight = 0

  override def reset: Unit = {
    if (id != -1) {
      glDeleteTextures(id)
      id = -1
    }
  }

  override def set(typ: Texture.Type, frmt: Texture.Format, width: Int, height: Int,
                   cubemapFace: Option[Texture.Face], buf: Option[ByteBuffer]) = {
    if (typ != texType && id != -1) {
      glDeleteTextures(id)
      id = -1
    }

    if (id == -1)
      id = glGenTextures

    println(s"creating texture $id as $typ")

    this.texType = typ
    this.texFormat = frmt
    this.texWidth = width
    this.texHeight = height

    val cubemapTarget = cubemapFace match {
      case Some(Texture.PositiveX) => Some(GL_TEXTURE_CUBE_MAP_POSITIVE_X)
      case Some(Texture.NegativeX) => Some(GL_TEXTURE_CUBE_MAP_NEGATIVE_X)
      case Some(Texture.PositiveY) => Some(GL_TEXTURE_CUBE_MAP_POSITIVE_Y)
      case Some(Texture.NegativeY) => Some(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y)
      case Some(Texture.PositiveZ) => Some(GL_TEXTURE_CUBE_MAP_POSITIVE_Z)
      case Some(Texture.NegativeZ) => Some(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z)
      case None => None
    }

    val bindTarget = bindTargetToGl(typ)

    val texTarget =  typ match {
      case Texture.Texture2D => GL_TEXTURE_2D
      case Texture.Cubemap => cubemapTarget.get
    }

    val texFormat = formatToGl(frmt)
    val dataFormat = if (frmt == Texture.DepthStencil) GL_DEPTH_STENCIL else texFormat
    val dataType = if (frmt == Texture.DepthStencil) GL_UNSIGNED_INT_24_8 else GL_UNSIGNED_BYTE

    bind(Texture.Unit0)
    buf match {
      case Some(buf) => glTexImage2D (texTarget, 0, texFormat, width, height, 0, dataFormat, dataType, buf)
      case None =>      glTexImage2D (texTarget, 0, texFormat, width, height, 0, dataFormat, dataType, 0)
    }
    unbind(Texture.Unit0)

    wrap(Texture.Repeat, Texture.Repeat, Texture.Repeat)
    filter(Texture.Nearest, Texture.Nearest)
  }

  override def bind(unit: Texture.Unit): Unit = {
    glActiveTexture(unitToEnum(unit))
    glBindTexture(bindTargetToGl(texType), id)
  }

  override def unbind(unit: Texture.Unit): Unit = {
    glActiveTexture(unitToEnum(unit))
    glBindTexture(bindTargetToGl(texType), 0)
  }

  def checker = {
    val buf = BufferUtils.createByteBuffer(2 * 2 * 4)

    val intBuf = buf.asIntBuffer()
    intBuf.put(0, 0xFFFFFFFF)
    intBuf.put(1, 0)
    intBuf.put(2, 0)
    intBuf.put(3, 0xFFFFFFFF)

    set(Texture.Texture2D, Texture.ColorRGBA, 2, 2, None, Some(buf))

    wrap(Texture.Repeat, Texture.Repeat, Texture.Repeat)
    filter(Texture.Nearest, Texture.Nearest)
  }

  def unitToEnum(unit: Texture.Unit) = unit match {
    case Texture.Unit0 => GL_TEXTURE1
    case Texture.Unit1 => GL_TEXTURE2
    case Texture.Unit2 => GL_TEXTURE3
    case Texture.Unit3 => GL_TEXTURE4
    case Texture.Unit4 => GL_TEXTURE5
    case Texture.Unit5 => GL_TEXTURE6
    case Texture.Unit6 => GL_TEXTURE7
    case Texture.Unit7 => GL_TEXTURE8
  }

  override def filter(min: Texture.Filter, max: Texture.Filter): Unit = {
    val target = bindTargetToGl(texType)
    bind(Texture.Unit0)
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filterModeToGl(min))
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filterModeToGl(max))
    unbind(Texture.Unit0)
  }

  override def wrap(x: Texture.Wrap, y: Texture.Wrap, z: Texture.Wrap): Unit = {
    val target = bindTargetToGl(texType)
    bind(Texture.Unit0)
    glTexParameteri(target, GL_TEXTURE_WRAP_S, wrapModeToGl(x))
    glTexParameteri(target, GL_TEXTURE_WRAP_T, wrapModeToGl(y))
    glTexParameteri(target, GL_TEXTURE_WRAP_R, wrapModeToGl(z))
    unbind(Texture.Unit0)
  }

  def filterModeToGl(mode: Texture.Filter) = mode match {
    case Texture.Nearest => GL_NEAREST
    case Texture.Linear => GL_LINEAR
  }

  def wrapModeToGl(mode: Texture.Wrap) = mode match {
    case Texture.Repeat => GL_REPEAT
    case Texture.Clamp => GL_CLAMP_TO_EDGE
  }

  private def bindTargetToGl(texType: Texture.Type) = texType match {
    case Texture.Texture2D => GL_TEXTURE_2D
    case Texture.Cubemap => GL_TEXTURE_CUBE_MAP
  }

  def formatToGl(format: Texture.Format): Int = format match {
    case Texture.ColorRGBA => GL_RGBA
    case Texture.ColorRGBA32F => GL_RGBA32F
    case Texture.DepthStencil => GL_DEPTH24_STENCIL8
  }

  def datatypeToGl(datatype: Texture.DataType): Int = datatype match {
    case Texture.UnsignedInt => GL_UNSIGNED_INT
  }
}
