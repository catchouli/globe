package globe.rendering.gl

import globe.rendering.RendererImpl
import globe.rendering.api.StateManager
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl._

import scala.collection.mutable

object GLRendererImpl extends RendererImpl {
  override def createShaderImpl = new GLShaderImpl
  override def createBufferImpl = new GLBufferImpl
  override def createShaderUniformImpl[A] = new GLUniformImpl[A]
  override def createShaderAttribImpl = new GLAttribImpl
  override def createStateImpl = new GLStateImpl
  override def createTexture = new GLTextureImpl
  override def createFramebuffer = new GLFramebufferImpl

  override def init(win: Long): Unit = {
    println(s"initialising opengl")
    // Initialise opengl
    glfwMakeContextCurrent(win)
    GL.createCapabilities()
    glEnable(GL_DEPTH_TEST)
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f)
  }

  override def startFrame = {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
  }

  override def endFrame = checkErr

  def checkErr = {
    val err = glGetError()
    if (err != GL_NO_ERROR) {
      throw new RuntimeException(s"GL error: $err")
    }
  }
}
