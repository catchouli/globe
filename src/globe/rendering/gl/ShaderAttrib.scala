package globe.rendering.gl

import globe.rendering.api.AttribImpl
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._

class GLAttribImpl extends AttribImpl {
  var location: Int = -1

  def enable() = {
    if (location != -1)
      glEnableVertexAttribArray(location)
  }

  def disable() = {
    if (location != -1)
      glDisableVertexAttribArray(location)
  }

  def bind(size: Int, stride: Int, offset: Int) = {
    if (location != -1)
      glVertexAttribPointer(location, size, GL_FLOAT, false, stride, offset)
  }

  def init(name: String) = {
    val program = glGetInteger(GL_CURRENT_PROGRAM)
    location = glGetAttribLocation(program, name)
  }
}