package globe.rendering.gl

import globe.rendering.api.{Texture, UniformImpl}
import org.joml.Matrix4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL20._

object GLBuffers {
  val float16buf = BufferUtils.createFloatBuffer(16)
}

class GLUniformImpl[A] extends UniformImpl[A] {
  var location: Int = -1

  def init(name: String) = {
    val prog = glGetInteger(GL_CURRENT_PROGRAM)
    location = glGetUniformLocation(prog, name)
  }

  override def set(value: A) = if (location != -1) value match {
    case m: Matrix4f => {
      m.get(GLBuffers.float16buf)
      glUniformMatrix4fv(location, false, GLBuffers.float16buf)
    }
    case u: Texture.Unit => {
      glUniform1i(location, unitToInt(u))
    }
  }

  def unitToInt(unit: Texture.Unit) = unit match {
    case Texture.Unit0 => 1
    case Texture.Unit1 => 2
    case Texture.Unit2 => 3
    case Texture.Unit3 => 4
    case Texture.Unit4 => 5
    case Texture.Unit5 => 6
    case Texture.Unit6 => 7
    case Texture.Unit7 => 8
  }

  private def size = 16
}