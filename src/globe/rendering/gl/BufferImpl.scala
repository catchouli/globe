package globe.rendering.gl

import java.nio.ByteBuffer

import globe.rendering.api._
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL15._

import scala.collection.mutable._

class GLBufferImpl extends BufferImpl {
  val vbo = glGenBuffers()

  def upload(target: VertexBuffer.BindTarget, data: ByteBuffer) = {
    glBindBuffer(glTarget(target), vbo)
    glBufferData(glTarget(target), data, GL_STATIC_DRAW)
  }

  def bind(target: VertexBuffer.BindTarget) = {
    glBindBuffer(glTarget(target), vbo)
  }

  private def glTarget(target: VertexBuffer.BindTarget) = target match {
    case VertexBuffer.VertexBuffer => GL_ARRAY_BUFFER
    case VertexBuffer.IndexBuffer => GL_ELEMENT_ARRAY_BUFFER
  }
}
