package globe.rendering.gl

import globe.rendering.api.{Primitive, Shader, ShaderImpl}
import globe.util.Resources
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL20._

class GLShaderImpl extends ShaderImpl {
  var id: Int = -1

  def bind = {
    glUseProgram(id)
  }

  def unbind = {
    glUseProgram(0)
  }

  def drawArrays(prim: Primitive.Type, start: Int, count: Int) = {
    glDrawArrays(primToEnum(prim), start, count)
  }

  def drawElements(prim: Primitive.Type, count: Int) = {
    glDrawElements(primToEnum(prim), count, GL_UNSIGNED_INT, 0)
  }

  def build(shader: Shader): Boolean = {
    // Generate program id
    if (id != -1)
      glDeleteProgram(id)
    id = glCreateProgram()

    // Try compiling our shader
    shader.vertexSource.foreach(tryCompile(_, GL_VERTEX_SHADER, id))
    shader.fragmentSource.foreach(tryCompile(_, GL_FRAGMENT_SHADER, id))

    // Link the program
    glLinkProgram(id)
    glValidateProgram(id)
    if (glGetProgrami(id, GL_LINK_STATUS) == GL_FALSE || glGetProgrami(id, GL_VALIDATE_STATUS) == GL_FALSE) {
      println(s"Shader link failed:\n${glGetProgramInfoLog(id)}")
      return false
    }
    else {
      // Bind attribs and uniforms
      shader.bind
      bindAttribs(shader)
      bindUniforms(shader)
      shader.unbind
      return true
    }
  }

  private def tryCompile(path: String, target: Int, program: Int): Unit = {
    val file = Resources.getSource(path)

    val header = s"#version 330\n#define BUILDING_${shaderType(target).toUpperCase}_SHADER\n"
    val source = header + file.mkString

    val shader = glCreateShader(target)
    glShaderSource(shader, source)
    glCompileShader(shader)
    if (glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE) {
      println(s"Shader compilation failed:\n${glGetShaderInfoLog(shader)}")
    }
    else {
      glAttachShader(program, shader)
    }

    file.close
  }

  private def bindAttribs(shader: Shader) = {
    shader.shaderAttribs.foreach { attrib => {
      attrib.init
    } }
  }

  private def bindUniforms(shader: Shader) = {
    shader.shaderUniforms.foreach { uniform => {
      uniform.init
    } }
  }

  private def shaderType(target: Int) = target match {
    case GL_VERTEX_SHADER => "vertex"
    case GL_FRAGMENT_SHADER => "fragment"
  }

  private def primToEnum(prim: Primitive.Type) = prim match {
    case Primitive.Points => GL_POINTS
    case Primitive.LineLoop => GL_LINE_LOOP
    case Primitive.Triangles => GL_TRIANGLES
    case Primitive.Quads => GL_QUADS
  }
}