#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec4 col;

out vec4 var_col;

uniform mat4 proj;
uniform mat4 view;

void main()
{
  gl_Position = proj * mat4(mat3(view)) * vec4(pos, 1.0);
  var_col = col;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec4 var_col;

out vec4 fragColor;

void main()
{
  float intensity = var_col.a;
  float brightness = pow(10.0, 0.4 * (3.0 - intensity));
  fragColor = vec4(var_col.rgb * brightness, 1.0);
}

#endif
