#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec3 nrm;
in vec3 col;
in vec2 uv;

out vec3 var_pos;
out vec3 var_nrm;
out vec3 var_col;
out vec2 var_uv;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 obj;

void main()
{
  gl_Position = proj * view * obj * vec4(pos, 1.0);
  var_nrm = nrm;
  var_col = col;
  var_uv = uv;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec3 var_pos;
in vec3 var_nrm;
in vec3 var_col;
in vec2 var_uv;

out vec4 fragColor;

void main()
{
  vec3 lightVec = normalize(vec3(-1.0, -1.0, -1.0));
  float d = dot(lightVec, var_nrm);
  d = d * 0.5 + 0.5;
  d = d * d;
  vec3 diffuse = d * vec3(1.0);
  fragColor = vec4(diffuse, 1.0);
}

#endif
