#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec3 col;

out vec3 var_col;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 obj;

void main()
{
  gl_Position = proj * view * obj * vec4(pos, 1.0);
  var_col = col;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec3 var_col;

out vec4 fragColor;

void main()
{
  fragColor = vec4(var_col, 1.0);
}

#endif
