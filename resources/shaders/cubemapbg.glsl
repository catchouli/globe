uniform mat4 proj;
uniform mat4 view;

#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec2 uv;

out vec2 var_uv;

void main()
{
  gl_Position = vec4(pos, 1.0);
  var_uv = uv;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec2 var_uv;

out vec4 fragColor;

uniform samplerCube tex;

vec3 ray(vec2 uv) {
  mat4 vp = proj * view;
  mat4 vpi = inverse(vp);

  vec2 pos = uv * 2.0 - 1.0;

  vec4 f = vpi * vec4(pos, -0.5, 1.0);
  vec3 start = f.xyz / f.w;

  f = vpi * vec4(pos, 0.5, 1.0);
  vec3 end = f.xyz / f.w;

  vec3 origin = start;
  vec3 direction = normalize(end - start);

  return direction;
}

void main()
{
  // in lieu of hdr
  float att = 0.2;
  vec3 dir = ray(var_uv);
  fragColor = vec4(att * texture(tex, dir).rgb, 1.0);
}

#endif
