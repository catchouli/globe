#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 obj;

void main()
{
  gl_Position = proj * view * obj * vec4(pos, 1.0);
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

out vec4 fragColor;

void main()
{
  fragColor = vec4(1.0);
}

#endif
