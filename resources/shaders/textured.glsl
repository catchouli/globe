#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec2 uv;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 obj;

out vec2 var_uv;

void main()
{
  gl_Position = vec4(pos.xy, 0.0, 1.0);
  var_uv = uv;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec2 var_uv;

out vec4 fragColor;

uniform sampler2D tex;

void main()
{
  fragColor = vec4(texture(tex, var_uv).rgb, 1.0);
}

#endif
