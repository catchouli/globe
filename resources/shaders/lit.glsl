#ifdef BUILDING_VERTEX_SHADER

in vec3 pos;
in vec3 nrm;

out vec3 var_nrm;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 obj;

void main()
{
  gl_Position = proj * view * obj * vec4(pos, 1.0);
  var_nrm = nrm;
}

#endif

#ifdef BUILDING_FRAGMENT_SHADER

in vec3 var_nrm;

out vec4 fragColor;

void main()
{
  vec3 lightVec = normalize(vec3(-1.0, -1.0, -1.0));
  float d = dot(lightVec, var_nrm);
  d = d * 0.5 + 0.5;
  d = d * d;
  fragColor = vec4(vec3(d), 1.0);
}

#endif
