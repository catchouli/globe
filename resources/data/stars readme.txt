The first 4 bytes of the file is the version (7 at the time of writing).
The next 4 bytes is the number of stars in the star table.

The rest of the file is a table of stars in layers, with the stars in the format:
4 bytes - layer
12 bytes - (x, y, z) as float32s
16 bytes - (r, g, b, magnitude) as float32s